function oneThroughTwenty() {
  for (let counter = 1; counter <= 20; counter++) {
    return counter;
  }
}
console.log(oneThroughTwenty());

function evensToTwenty() {
  for (let a = 2; a <= 20; a + 2)  {
    return a;
  }
}
console.log(evensToTwenty());

function oddsToTwenty() {
  for (let b = 1; b <= 20; b + 2)  {
    return b;
  }
}
console.log(oddsToTwenty());

function multiplesOfFive() {
  for (let c = 1; c <= 100; c * 5)  {
    return c;
  }
}
console.log(multiplesOfFive());

function squareNumbers() {
  for (let d = 1; d <= 10; d++)  {
    return d * d;
}
console.log(squareNumbers());

function countingBackwards() {
  for (let e = 20; e >= 1; e--)   {
  return e;
  }
}
console.log(countingBackwards());

function evenNumbersBackwards() {
  for (let f = 20; f >= 1; f -= 2)  {
    return f;
  }
}
console.log(evenNumbersBackwards());

function oddNumbersBackwards() {
  for (let g = 19; g >= 1; g -= 1)  {
    return g;
  }
}
console.log(oddNumbersBackwards());

function multiplesOfFiveBackwards() {
  for (let h = 100; h >= 5; h / 5)  {
    return h;
  }
}
console.log(multiplesOfFiveBackwards());

function squareNumbersBackwards() {
  for (let i = 10; i >= 1; i--)  {
    return i * i;
  }
}
console.log(squareNumbersBackwards());